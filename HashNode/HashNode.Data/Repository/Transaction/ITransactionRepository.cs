﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HashNode.Data.Repository.Transaction
{
    public interface ITransactionRepository
    {
        IEnumerable<tblTransaction> GetTransactions(string senderPublicKey);

        tblTransaction GetTransaction(string ParentHashId);
    }
}
