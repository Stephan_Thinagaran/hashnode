﻿using System.Collections.Generic;
using System.Data.Entity;

namespace HashNode.Data.Repository.Transaction
{
    public class TransactionRepository : ITransactionRepository
    {
        private readonly AspireRewardsEntities _dbContext;

        public delegate ITransactionRepository Factory(AspireRewardsEntities dbContext);

        public TransactionRepository(AspireRewardsEntities dbContext)
        {
            _dbContext = dbContext;
        }

        IEnumerable<tblTransaction> ITransactionRepository.GetTransactions(string senderPublicKey)
        {
            throw new System.NotImplementedException();
        }

        tblTransaction ITransactionRepository.GetTransaction(string ParentHashId)
        {
            throw new System.NotImplementedException();
        }
    }
}
