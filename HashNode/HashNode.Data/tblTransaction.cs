//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HashNode.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblTransaction
    {
        public string HashId { get; set; }
        public string PrevHashId { get; set; }
        public string TransactionId { get; set; }
        public string Message { get; set; }
        public string PublicKey { get; set; }
        public string DigitalSignature { get; set; }
        public string Index { get; set; }
        public string Nonce { get; set; }
        public System.DateTime us_DateCreate { get; set; }
        public string us_WhoCreate { get; set; }
        public System.DateTime us_DateModified { get; set; }
        public string us_WhoModified { get; set; }
    }
}
