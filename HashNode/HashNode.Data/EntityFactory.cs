﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HashNode.Data
{
    public class EntityFactory
    {
        private readonly string connectionString = "";

        public EntityFactory()
        {
            connectionString = ConfigurationManager.ConnectionStrings["AspireRewardsEntities"].ConnectionString;
        }

        public AspireRewardsEntities CreateEntites()
        {
            return new AspireRewardsEntities(connectionString);
        }
    }
}
