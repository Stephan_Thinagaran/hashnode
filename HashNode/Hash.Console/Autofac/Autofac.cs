﻿using Autofac;
using HashNode.Business.MineBlock;
using HashNode.Business.TransactionHandler;
using HashNode.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading.Tasks;

namespace Hash.Console.Autofac
{
    public class Autofac
    {
        public static IContainer ConfigureContainer()
        {
            var builder = new ContainerBuilder();

            //Register dependencies here
            builder.RegisterType<AspireRewardsEntities>().InstancePerDependency();
            builder.RegisterType<MineBlock>().AsImplementedInterfaces().InstancePerDependency();
            builder.RegisterType<TransactionHandler>().AsImplementedInterfaces().InstancePerDependency();
            var container = builder.Build();
            return container;
        }
    }
}
