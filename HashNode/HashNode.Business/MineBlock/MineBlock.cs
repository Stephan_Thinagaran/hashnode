﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace HashNode.Business.MineBlock
{
    public class MineBlock
    {
        private string _index;
        private string _prevHash;
        private string _timeStamp;
        private string _data;
        private string _hash;
        private Int64 _nonce;

        public void MineBlockProcess(string index, string timestamp, string data, string previousHash, int difficulty)
        {
            //set values
            _index = index;
            _prevHash = previousHash;
            _timeStamp = timestamp;
            _data = data;
            _hash = CalculateHash();
            _nonce = 0;

            string[] array = new string[difficulty + 1];
            string value = string.Join("0", array);

            while (this._hash.Substring(0, difficulty) != value)
            {
                _nonce++;
                _hash = CalculateHash();
            }
        }

        public string CalculateHash()
        {
            ASCIIEncoding encoding = new ASCIIEncoding();
            byte[] encodedBytes = encoding.GetBytes(this._index
                                    + _prevHash
                                    + _timeStamp
                                    + JsonConvert.SerializeObject(this._data)
                                    + _nonce.ToString()
                                    );

            var algorithm = new HMACSHA256();
            var hash = algorithm.ComputeHash(encodedBytes);

            return Convert.ToBase64String(hash);
        }

    }
}
