﻿using HashNode.Business.MineBlock;
using HashNode.Data;
using HashNode.Data.Repository.Transaction;
using IrohaDotNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HashNode.Business.TransactionHandler
{
    public class TransactionHandler : ITransactionHandler
    {
        private readonly IMineBlock _mineBlock;
        private readonly ITransactionRepository _transactionRepository;
        public delegate ITransactionHandler Factory(IMineBlock mineBlock, ITransactionRepository transactionRepository);

        public TransactionHandler(IMineBlock mineBlock, ITransactionRepository transactionRepository)
        {
            _mineBlock = mineBlock;
            _transactionRepository = transactionRepository;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="senderPublicKey"></param>
        /// <param name="receiverPublicKey"></param>
        /// <param name="message"></param>
        /// <param name="digitalSignature"></param>
        public void Process(string senderPublicKey, string receiverPublicKey, string message, string digitalSignature)
        {
            try
            {
                //Verify signature
                if (VerifySignature(receiverPublicKey, message, digitalSignature))
                {
                    //Get all Transactions
                    var transactions = _transactionRepository.GetTransactions(senderPublicKey);

                    if (ValidateBlockChain(transactions))
                        Console.WriteLine("Add this block");
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine($"Invalid Trasaction - {ex.Message}");
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="receiverPublicKey"></param>
        /// <param name="message"></param>
        /// <param name="digitalSignature"></param>
        /// <returns></returns>
        private bool VerifySignature(string receiverPublicKey, string message, string digitalSignature)
        {
            string encryptMessage = Sha3Util.Sha3_256(message);

            return Iroha.Verify(receiverPublicKey, digitalSignature, encryptMessage);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="records"></param>
        /// <returns></returns>
        private bool ValidateBlockChain(IEnumerable<tblTransaction> records)
        {
            foreach (var transaction in records)
            {
                if (!(ValidateBlock(transaction)))
                    return false;
            }

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="record"></param>
        /// <returns></returns>
        private bool ValidateBlock(tblTransaction record)
        {
            bool isValidBlock = false;

            //Verify signature in the block
            if (VerifySignature(record.PublicKey, record.Message, record.DigitalSignature))
            {
                if (Convert.ToInt32(record.Index) != 0)
                {
                    //Verify previous hash exists
                    var parentTrasaction = _transactionRepository.GetTransaction(record.PrevHashId);

                    //Make sure parentTransaction Balance is zero
                    //Parent trasaction current value must be 
                    //greater than or equal to child transaction current
                }
            }

            return isValidBlock;
        }
    }
}
