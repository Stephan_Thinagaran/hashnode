IF NOT EXISTS(SELECT so.Name AS FieldName FROM sysobjects AS so WHERE SO.name = 'tblTransaction')
BEGIN

CREATE TABLE [dbo].[tblTransaction](
[HashId] [varchar](500) NOT NULL,
[PrevHashId] [varchar](500) NOT NULL,
[TransactionId] [varchar](500) NOT NULL,
[Message] [varchar](500) NOT NULL,
[PublicKey] [varchar](500) NOT NULL,
[DigitalSignature] [varchar](500) NOT NULL,
[Index] [varchar](500) NOT NULL,
[Nonce] [varchar](500) NOT NULL,
[us_DateCreate] [datetime] NOT NULL,
[us_WhoCreate] [char](50) NOT NULL,
[us_DateModified] [datetime] NOT NULL,
[us_WhoModified] [char](50) NOT NULL,
 CONSTRAINT [PK_tblTransaction_HashId] PRIMARY KEY CLUSTERED 
(
[HashId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] 
END